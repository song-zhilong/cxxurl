/**
 * @author : xiaozhuai
 * @date   : 17/1/3
 */

#ifndef CXXURL_VERSION_H
#define CXXURL_VERSION_H

#define CXX_URL_VERSION         "0.0.1"

#endif //CXXURL_VERSION_H
